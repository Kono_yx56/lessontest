package org.itstep.qa.lesson;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LessonTest {

    private WebDriver driver;

    @BeforeClass
    public void createBrowser(){
        System.setProperty("webdriver.chrome.driver",
                "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser(){
        driver.quit();
    }

    @BeforeMethod
    public void clear(){
        driver.manage().deleteAllCookies();
    }

    @Test
    public void findInGoogleTest(){
        driver.get("https://www.google.ru");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("кот" + Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"rso\"]/div[2]/div/div/div/div[1]/a/h3"));
        Assert.assertEquals("Кошка — Википедия", element.getText());
    }

    @Test
    public void sendToMoscow(){
        driver.get("http://hflabs.github.io/suggestions-demo/");
        WebElement element = driver.findElement(By.id("fullname-surname"));
        element.sendKeys("Петров");
        element = driver.findElement(By.id("fullname-name"));
        element.sendKeys("Иван");
        element = driver.findElement(By.id("email"));
        element.sendKeys("ivan@petrov.ru");
        element = driver.findElement(By.id("message"));
        element.sendKeys("Очень благодарен");
        driver.findElement(By.cssSelector(".btn-primary")).click();
        //проверка результата
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/h4"));
        Assert.assertEquals(element.getText(), "Это не настоящее правительство :-(");
        element = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[1]"));
        Assert.assertEquals(element.getText(), "К сожалению, мы не можем принять ваше обращение.\n" +
                "Но вы всегда можете отправить его через электронную приемную правительства Москвы.");
    }
}
